# This is where the magic happens

import os
from pathlib import Path


if os.getenv("FLATPAK_ID"):
    locations = [
        Path.home() / ".themes",
        Path.home() / ".local" / "share" / "themes",
        Path("/var/run/host") / "usr" / "share" / "themes",
    ]
else:
    locations = [
        Path.home() / ".themes",
        Path.home() / ".local" / "share" / "themes",
        Path("/") / "usr" / "share" / "themes",
    ]
directory_list = ["assets", "apps", "widgets", "window-assets",]
file_list = [
    "gtk.css",
    "gtk-dark.css",
]
config_path = Path.home() / ".config" / "gtk-4.0"


def find_theme(theme_name: str):
    if theme_name == "":
        raise ValueError("Theme name is required")

    for location in locations:
        path = location / theme_name
        if path.exists():
            return path
    raise FileNotFoundError("No theme found by that name")

def link_theme(theme_location: Path):
    source_path = theme_location / "gtk-4.0"
    dest_path = config_path

    for directory in directory_list:
        source = source_path / directory
        destination = dest_path / directory
        os.symlink(source, destination)
    for file in file_list:
        os.symlink(source_path / file, dest_path / file)

def move_existing():
    for resource in directory_list + file_list:
        path = config_path / resource
        path.rename(path.with_suffix(".bak"))

def get_theme_list():
    theme_list = []
    for location in locations:
        for directory in location.iterdir():
            gtk_dir = directory / "gtk-4.0"
            if gtk_dir.exists():
                theme_list.append(directory.name)
    theme_list.sort()
    return theme_list

