# window.py
#
# Copyright 2023 Sky Barnes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import Gio

from .util import get_theme_list

@Gtk.Template(resource_path='/com/tsbarnes/AdwThemeSwitcher/window.ui')
class AdwthemeswitcherWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'AdwthemeswitcherWindow'

    theme_dropdown = Gtk.Template.Child()
    theme_list = Gtk.Template.Child()
    apply_button = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        application = self.get_application()

        settings = Gio.Settings.new('org.gnome.desktop.interface')
        current_theme = settings.get_string('gtk-theme')

        self.apply_button.connect('clicked', application.apply)

        position = 0
        theme_list = get_theme_list()
        for index in range(0, len(theme_list)):
            if theme_list[index] == current_theme:
                position = index
            self.theme_list.append(theme_list[index])

        self.theme_dropdown.set_selected(position)

