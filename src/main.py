# main.py
#
# Copyright 2023 Sky Barnes
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')

from gi.repository import Gtk, Gio, Adw
from .window import AdwthemeswitcherWindow
from .util import find_theme, link_theme, move_existing


class AdwthemeswitcherApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id='com.tsbarnes.AdwThemeSwitcher',
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        self.create_action('quit', lambda *_: self.quit(), ['<primary>q'])
        self.create_action('about', self.on_about_action)
        self.create_action('preferences', self.on_preferences_action)

    def display_message(self, message: str, message_type: Gtk.MessageType = Gtk.MessageType.INFO, buttons: Gtk.ButtonsType = Gtk.ButtonsType.OK):
        dialog = Gtk.MessageDialog(
            transient_for=self.props.active_window,
            message_type=message_type,
            buttons=buttons,
            text=message
        )
        dialog.set_modal(True)
        dialog.connect('response', self.widget_destroy_self)
        dialog.show()

    def widget_destroy_self(self, widget, _):
        widget.destroy()

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = AdwthemeswitcherWindow(application=self)
        win.present()

    def apply(self, widget):
        """Called when the button is clicked

        Handles the linking of the theme to the GTK4 config
        directory and backing up the old links
        """
        theme_dropdown = self.props.active_window.theme_dropdown

        theme_name = theme_dropdown.get_selected_item()
        if not theme_name:
            self.display_message("No theme specified", Gtk.MessageType.ERROR)
            return

        try:
            theme = find_theme(theme_name.get_string())
        except FileNotFoundError:
            text = "Couldn't find theme '{}'".format(theme_name)
            self.display_message(text, Gtk.MessageType.ERROR)
            return

        settings = Gio.Settings.new('org.gnome.desktop.interface')
        settings.set_string('gtk-theme', theme_name.get_string())
        settings.sync()

        try:
            move_existing()
        except OSError:
            text = "Error moving old theme files"
            self.display_message(text, Gtk.MessageType.ERROR)

        try:
            link_theme(theme)
        except OSError:
            text = "Failed to create symbolic links"
            self.display_message(text, Gtk.MessageType.ERROR)
            return

        text = "Theme linked successfully!"
        self.display_message(text)

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name='adwthemeswitcher',
                                application_icon='com.tsbarnes.AdwThemeSwitcher',
                                developer_name='Sky Barnes',
                                version='0.1.0',
                                developers=['Sky Barnes'],
                                copyright='© 2023 Sky Barnes')
        about.present()

    def on_preferences_action(self, widget, _):
        """Callback for the app.preferences action."""
        print('app.preferences action activated')

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    app = AdwthemeswitcherApplication()
    return app.run(sys.argv)
